# HOME PROJECT TO IMPROVE MOTION APPLICATION

++ Ref page:

https://motion-project.github.io/motion_config.html

https://www.instructables.com/id/Raspberry-Pi-Motion-Detection-Security-Camera/

++ My motivation: To hear my new born daughter from my company.

++ HW list:
Raspberry Pi 3B
Logitech USB Camera C270 (with microphone)

++ SW tasks:
1) Setting up motion SW to work with C270
2) Setting up microphone recording
3) Merging video (output by motion) and recorded audio.
4) Handle free space issue and clean up temporary audio/video files
5) Schedule reboot daily, to make sure the temporary Linux stuffs are cleaned.